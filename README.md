# Web Scraping Tutorial
Qixuan Yang (Update: 25th of Feburary, 2019)

PLSC 799 Microhistorical Analysis in Social Science Research, Prof. Isabela Mares

Department of Political Science, Yale University

## Instruction

Please download this repository by clicking the cloud symbol next to "Find file" above as a .zip file. Please unzip the folder on your local computer.

The repository features the following documents

- ``README.md``: Instruction file
- ``illustration.html``: Hand-out based on html (can be exported as PDF, print-friendly)
- ``illustration.ipynb``: Hand-out based on ipynb with R-kernel
- ``script.r``: The working R code for the tutorial

Prerequisite:
1. Please have R (https://cloud.r-project.org) and R-Studio (https://www.rstudio.com/products/rstudio/download/, Free version) installed. 
2. Please have Chrome and the Chrome extension SelectorGadget (https://chrome.google.com/webstore/detail/selectorgadget/mhjhnkcfbdhnjickkkdbjoemdmbfginb?hl=en) installed (Except you are familiar with CSS). 
3. Please also have the packages dplyr and rvest installed (using ``install.packages("package-name")``). 
4. For those who haven't done R or other scripting language before, the free tutorial on Data Camp (https://www.datacamp.com/courses/free-introduction-to-r) could be helpful.

For those who cannot attend the tutorial, the following resources might be helpful to understand how selector gadget works, and how to combine it with rvest:

- [Demonstration of selector gadget](https://selectorgadget.com)
- [A post on RStudio Blog about how to use rvest](https://blog.rstudio.com/2014/11/24/rvest-easy-web-scraping-with-r/)

## Notes
This web-scraping tutorial uses the Obama archive (https://obamawhitehouse.archives.gov) as the example. Regarding the copyrights for these document in academic research and other non-commercial usage, please refer to the instruction of its homepage.

I am very grateful for the development simple tool [SelectorGadget](https://selectorgadget.com), which facilitates the identification of typical nodes and attributes of a website.

## License
<a href="http://www.wtfpl.net/"><img
       src="http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-4.png"
       width="88" height="31" alt="WTFPL" /></a>