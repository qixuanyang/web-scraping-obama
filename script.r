# Web-scraping techniques: Strategy and Intuitions
# PLSC 799 Microhistorical Analysis in Social Science Research, Prof. Isabela Mares

## Preparation-------------------------------------------------------------

# If the packages are not installed, please do (By excluding the hashtag)
# install.packages("dplyr")
# install.packages("rvest")

## Loading packages and explore websites-----------------------------------

library(dplyr) # Data manipulation and function chaining (explained later)
library(rvest) # A powerful tool to extract web content, motivated by beautiful soup in Python

browseURL("https://obamawhitehouse.archives.gov/briefing-room/speeches-and-remarks")
# Speech and remarks website -> pages -> links -> interested web-page content (which has, again, different (nested) parts)

# We observe an interesting fact that the page number has a logic starting on page 2
paste0("https://obamawhitehouse.archives.gov/briefing-room/speeches-and-remarks?term_node_tid_depth=31&page=1")

# What if we try "page = 0"?
browseURL("https://obamawhitehouse.archives.gov/briefing-room/speeches-and-remarks?term_node_tid_depth=31&page=0")
# We get the first page!

# Upon this observation and based on the fact that there are 473 pages, we
# can generate the following vector of webpages

pages <- c(1:437)-1
prefix <- "https://obamawhitehouse.archives.gov/briefing-room/speeches-and-remarks?term_node_tid_depth=31&page="
container_pages <- paste0(prefix, pages)

head(container_pages)

## Testing on a unit ------------------------------------------------------

# Test on one page: read_html to extract the content of the webpage
temp <- container_pages[1]
temp <- read_html(temp)

temp

# Select the attribute: .field-content
html_nodes(temp, ".field-content")

# Alternatively using the chain provided by dplyr
# Very powerful when it comes to complicated extraction
temp %>% html_nodes(".field-content")

# See what is in <a> ... </a>
temp %>% html_nodes(".field-content a")

# Extracting the text in <a> ... </a>
temp %>% html_nodes(".field-content a") %>% html_text()
# The titles!
# Equivalent: temp %>% html_nodes(".field-content") %>% html_nodes("a")

# Extracting the links in <a> ... </a>
temp %>% html_nodes(".field-content a") %>% html_attr("href")
# Not a standard url - No problem, we will see how to inspect the prefix of it

# Extracting the date
temp %>% html_nodes(".field-content") %>% html_text()

## Recap -------------------------------------------------------

# Briefly summarize

# Read html
temp <- container_pages[1] %>% read_html()

# Get the things we are interested in
links <- temp %>% html_nodes(".field-content a") %>% html_attr("href")
texts <- temp %>% html_nodes(".field-content") %>% html_text()

# Observe that in "texts", date is in the odd line whereas title in the even line

    #' Example: seq(start, end, steps) or seq(start, end, length)
    sep(1,8,2)
    sep(1,9,3)
    sep(1,9,length.out = 5)

dates <- texts[seq(1, length(texts), 2)] # Sequence: 1, 3, 5, ... till the length of a vector
titles <- texts[seq(2, length(texts), 2)] # Sequence: 2, 4, 6, ... till the length of a vector

data.frame(links, dates, titles)

###################### Exercise time #######################
################# Debates in UK Parliament #################
##################### House of Commons #####################

# Website
browseURL(https://hansard.parliament.uk/search/Debates?house=commons)

# Task 1: Observe the website with the SelectorGadget. Create two containers
#
# 1.1: All guiding-webpage links from 26/02/2014 to 26/02/2019
# 1.2: All guiding-webpage links from 01/01/1800 to 26/02/2019 (Click on View all Years and you get this time range)

container_uk_1
container_uk_2

# Task 2: In the container_uk_1, select a webpage to extract the following information and store them accordingly
#
# title_uk: The titles of the document
# date_uk: Exact dates of the document release
# link_uk: Links to the document web-page ()

title_uk
date_uk
link_uk

###################### Exercise end #######################


## Writing a function ----------------------------------------------------

    #' Example: writing a function that add two numbers
    #' In R, we add two numbers by calling for instance
    2 + 3
    #' If we want to formalize it, we need to abstract the numbers to arguments (symbols)
    a <- 2
    b <- 3
    a + b
    #' We can see that by changing a and b, we generalize the any summation of two real numbers.
    #' In a function form
    sum_func <- function(a, b){ # Defining the arguments
        a <- a
        b <- b
        result <- a + b
        return(a + b)
    }
    #' Test:
    sum_func(2,3)
    sum_func(1,100)

# Let us write a function, with a container webpage as the single argument
create_data <- function(container, p){
    
    # Read html, 
    temp <- container[p] %>% read_html()

    # Get the things we are interested in
    links <- temp %>% html_nodes(".field-content a") %>% html_attr("href")
    texts <- temp %>% html_nodes(".field-content") %>% html_text()

    # Observe that in "texts", date is in the odd line whereas title in the even line
    dates <- texts[seq(1, length(texts), 2)] # Sequence: 1, 3, 5, ... till the length of a vector
    titles <- texts[seq(2, length(texts), 2)] # Sequence: 2, 4, 6, ... till the length of a vector
    
    # Add the container page as a reference
    page <- p
    
    df <- data.frame(links, dates, titles, page)
    return(df)
}

create_data(container_pages, 100)

list_data <- list()

    # Writing a loop (reminder: plotting the flow chart on the white board)

for(i in 1:10){
    list_data[[i]] <- create_data(container_pages, i)
    cat(i, "page is done. \n")
}

# More advanced version using sapply (or other parallelzing functions)
# list_data <- lapply(1:10, function(i) create_data(container_pages, i))

# Combine all the pages

df <- do.call(rbind, list_data)
head(df,15)

# I would suggest that advanced R-users use bind_rows(), because sometimes not all the pages have the
# same format. bind_rows() does a great job in generalizing that.

######################################################################################
######################################################################################
######################################################################################

## Scraping the individual webpage ----------------------------------------------------

# A typical link
# Observation: The links in our self-generated data frame miss "https://obamawhitehouse.archives.gov". We can just simply
# append it.

paste0("https://obamawhitehouse.archives.gov/the-press-office/2016/12/24/mensaje-semanal-feliz-navidad-del-presidente-y-la-primera-dama")

paste0("https://obamawhitehouse.archives.gov", df$links) %>% head()

# For advanced users: Use stringr package if you are handling regular expressions.

# Commit the adjustment

df$links <- paste0("https://obamawhitehouse.archives.gov", df$links)

# Again, let us take one link for instance

temp <- df$links[1] %>% read_html()

# Heading
temp %>% html_node("#press_article_heading_title") %>% html_text()

# Actor
temp %>% html_node("#press_article_heading_subtitle") %>% html_text()

temp %>% html_node("#content-start .even") %>% html_text() %>% strtrim(500)

temp %>% html_node("#content-start .even") %>% html_text() %>% strtrim(500) %>% cat()

###################### Exercise time #######################
################# Debates in UK Parliament #################
##################### House of Commons #####################

# Task: Select a webpage in container_uk_1, and save it as temp_uk
# Create a dataset which creates the following format:
# |--------------|----------------|
# | actor_name   | content_spoken |
# | actor_name   | content_spoken |
# | ...          | ...
# |--------------|----------------|
#
# Hint: Please save actor_name and content_spoken as separate vectors, and then
# use rbind() to concantenate them together in a data.frame() or matrix()

temp_uk
actor_name
content_spoken

###################### Exercise end #######################


## Formalizing with functions ----------------------------------------------------

# Summarize in a function:

create_individual_data <- function(container, p){
    links <- container[p]
    temp <- links %>% read_html()
    headings <- temp %>% html_node("#press_article_heading_subtitle") %>% html_text()
    actors <- temp %>% html_node("#press_article_heading_subtitle") %>% html_text()
    contents <- temp %>% html_node("#content-start .even") %>% html_text()
    
    ind_page <- p
    
    df <- data.frame(links, headings, actors, contents, ind_page)
    return(df)
}

list_data <- list()

for(i in 1:length(df$links)){
    list_data[[i]] <- create_individual_data(df$links, i)
    #Sys.sleep(2) # Pause for 2 seconds, you need such pause in many websites. But strangely, there is no limitation here..
    
    if(i %% 20 == 0){
        cat(i, "done \n")
    }
}

df_ind <- do.call(rbind, list_data)

# Take-home exercise (not obligatory): How to write a similar function to download
# all the debates of House of Commons in UK, based on the your codes in the in-class
# exercise and the example functions shown above?

## Combining and write data ----------------------------------------------------

df_all <- left_join(df, df_ind, by = c("links" = "links"))
write.csv(df_all, "obama.csv")
